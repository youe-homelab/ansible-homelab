<?php
$CONFIG = array (
  'datadirectory' => '/data',
  'dbtype' => 'sqlite3',
  'instanceid' => 'oc9e0qvuq9hr',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'overwrite.cli.url' => 'https://nextcloud.youe.fr',
  'overwriteprotocol' => 'https',
  'passwordsalt' => 'lRRqUgWIB2lyjQSWkMUpwnKcN5SSUI',
  'secret' => '7yhKwWjHSD7gFsY/1LqMBH9ih8KMiV3Zum7QbSEvXx5ei96y',
  'version' => '23.0.4.1',
  'trusted_domains' =>
  array (
    0 => '192.168.0.10',
    1 => 'nextcloud.youe.fr',
  ),
  'trusted_proxies' =>
  array (
    0 => '192.168.0.10',
    1 => 'nextcloud.youe.fr',
  ),
);
