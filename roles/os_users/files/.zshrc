source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh

prompt off
eval "$(starship init zsh)"

# Environment variables
export DISPLAY=localhost:0.0
export EDITOR="nvim"
export VISUAL="nvim"
export NNN_TRASH=1
export NNN_OPTS="aAdeEHoU"

# Aliases
alias sudo="sudo "
alias reboot="sudo systemctl reboot"
alias ls="ls -Alh --group-directories-first --color=auto"
alias rm="echo 'rm is disabled, use trash or /bin/rm instead.'"
alias n="nnn"
alias us="systemctl --user"
alias trash-empty="trash-list && trash-empty --all-users -f"
alias ncdu="ncdu -x --si --color dark --exclude-kernfs"
