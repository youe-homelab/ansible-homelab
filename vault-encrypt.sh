#!/usr/bin/env bash

ansible-vault encrypt_string --vault-password-file ~/vault_pass.txt "$2" --name "$1"
