#!/usr/bin/env bash

if [ ! -f ~/vault_pass.txt ]; then
  read -r -s -p "Password : " password
  printf "\nPassword saved\n"
  echo "$password" > ~/vault_pass.txt
fi

# ansible-galaxy install -r config/requirements.yml
ansible-playbook $2 --vault-password-file ~/vault_pass.txt "$1"
