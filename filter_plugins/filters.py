import os
import re

import yaml
from werkzeug.security import generate_password_hash


class FilterModule(object):
    def filters(self):
        return {
            "build_password": self.build_password,
            "werkzeug_hash": self.werkzeug_hash,
            "format_volumes": self.format_volumes,
            "kube_handle_yaml": self.kube_handle_yaml,
        }

    def build_password(self, src: str, key: str, ext: str = None) -> str:
        splits = src.split(".")
        result = key + splits[0]
        if len(splits) > 1:
            result += "." + splits[1].upper()
        if ext:
            result += "." + ext.upper()
        return result

    def werkzeug_hash(self, src) -> str:
        return generate_password_hash(src)

    def format_volumes(self, volumes: list[str], root: str) -> list[str]:
        result = []
        for v in volumes:
            if v.startswith("./"):
                v = v.replace("./", f"{root}", 1)
            if v.count(":") != 2:
                v += ":z"
            result.append(v)
        return result

    def kube_handle_yaml(self, path: str, path_out: str) -> dict:
        result = {
            "path": "",
        }
        with open(path, "r") as f:
            p: dict = yaml.safe_load(f)
        p["metadata"]["annotations"] = p["metadata"].get("annotations", {})
        p["spec"]["volumes"] = p["spec"].get("volumes", [])
        p["spec"]["restartPolicy"] = "OnFailure"
        for c in p["spec"]["containers"]:
            p["metadata"]["annotations"][
                "io.containers.autoupdate/" + c["name"]
            ] = "registry"
            for hp in c.get("ports", []):
                hp["hostPort"] = int(hp["hostPort"])
            for v in c.get("volumeMounts", []):
                v_names = v["name"].split("-")
                v_path = os.path.join(*([path_out] + v_names))
                if v_names[0] == p["metadata"]["name"] and v["name"] not in [n["name"] for n in p["spec"]["volumes"]]:
                    v_is_file = "." in v["mountPath"].split("/")[-1]
                    p["metadata"]["annotations"]["bind-mount-options:" + v_path] = "z"
                    p["spec"]["volumes"].append(
                        {
                            "hostPath": {
                                "path": v_path,
                                "type": "FileOrCreate" if v_is_file else "DirectoryOrCreate",
                            },
                            "name": v["name"],
                        }
                    )

        result["path"] = path.replace(".yaml", ".compiled.yaml")
        with open(result["path"], "w+") as f:
            if yaml.safe_load(f) != p:
                yaml.safe_dump(p, f)
        return result
