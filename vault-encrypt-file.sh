#!/usr/bin/env bash

ansible-vault encrypt --vault-password-file ~/vault_pass.txt "$1"
