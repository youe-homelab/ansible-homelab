from __future__ import absolute_import, division, print_function

import re

import requests

__metaclass__ = type

from ansible.module_utils.basic import AnsibleModule

mode_to_status = {
    1: "Online",
    2: "Maintenance With Notifications",
    3: "Maintenance Without Notifications",
}

def GET(url) -> dict:
    return requests.get(f"https://api.hetrixtools.com/{url}").json()


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        name_regex=dict(type="str", required=True),
        token=dict(type="str", required=True),
        mode=dict(type="int", required=True),
    )
    result = dict(changed=False)

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)
    if module.check_mode:
        module.exit_json(**result)

    params : dict = module.params
    token = params['token']
    monitors = GET(f"v1/{token}/uptime/monitors/0/30/")

    if "status" in monitors and monitors["status"] == "ERROR":
        module.warn(monitors["error_message"])
        module.exit_json(**result)

    for m in monitors[0]:
        if re.search(params["name_regex"], m["Name"]) != None:
            if m["Status"] != mode_to_status[params["mode"]]:
                if (
                    GET(
                        f"v2/{token}/maintenance/{m['ID']}/{params['mode']}/"
                    )["status"]
                    == "SUCCESS"
                ):
                    result["changed"] = True

    module.exit_json(**result)


def main():
    run_module()


if __name__ == "__main__":
    main()
