git add --all

# Push force if positional argument --amend is used
if ($1 = "--amend") {
  git commit --amend --no-edit
}
else {
  git commit -m "$1"
}

git push --force-with-lease
